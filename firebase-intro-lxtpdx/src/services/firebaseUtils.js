import * as firebaseApp from 'firebase'

const firebaseConfiguration = {
    apiKey: "AIzaSyAHCzLP68K4lf9AkTxGl608wgjNyB7PJHU",
    authDomain: "latinxtechpdxdemo.firebaseapp.com",
    databaseURL: "https://latinxtechpdxdemo.firebaseio.com",
    projectId: "latinxtechpdxdemo",
    storageBucket: "latinxtechpdxdemo.appspot.com",
    messagingSenderId: "873168297160"
}

export const firebaseUtils = firebaseApp.initializeApp(firebaseConfiguration)