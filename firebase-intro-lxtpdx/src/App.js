import React, { Component } from 'react'

import firebase from 'firebase'
import { firebaseUtils } from './services/firebaseUtils'

import logo from './logo.svg'
import './App.css'



class App extends Component {

  constructor(props){
    super(props)
    this.state = {
      members: {},
      email: '',
      username: '',
      password: ''
    }
  }

  componentWillMount(){
    let membersRef = firebaseUtils.database().ref('members')
    membersRef.on('value', (snapshot) => {
      let members = snapshot.val()
      console.log("Type of members ", typeof members)
      this.setState({
        members
      })
    })
  }

  addMember() {
    console.log("Clicked to add tech")

    let membersRef = firebaseUtils.database().ref('members')
    let key = membersRef.push().key;
    membersRef.child(key).set({
      'email': this.state.email
    })

    // reset input
    this.setState({email:''})
  }

  updateValue() {
    const userKey = '-KxKTx1fmyQmqHfIfMNU'
    firebaseUtils.database().ref('members/' + userKey + '/age').set(40)
  }

  deleteItem() {
    const keyToDelete = '-KxKTctjp4nEglXerupm'
    firebaseUtils.database().ref('members/' + keyToDelete).remove()
  }

  signUp() {
    let {username, password} = this.state
    firebaseUtils.auth().createUserWithEmailAndPassword(username, password)
    .then((firebaseUser) => {
        let uid = firebaseUser.uid
        console.log("Created new user ", uid)
        firebaseUser.sendEmailVerification()
    })
    .catch((error) => {
        console.log(error.message)
    });
  }

  login() {
    let {username, password} = this.state
    firebaseUtils.auth().signInWithEmailAndPassword(username, password)
    .then(() => {
        let firebaseUser = firebaseUtils.auth().currentUser;
        if (firebaseUser.emailVerified) {
            console.log("User has already verified email")
        }
        else{
          console.log("User has NOT YET verified email")
        }
    })
    .catch((error) => {
        console.log("Error during login ", error.message)
    })
  }

  logout() {
    firebaseUtils.auth().signOut()
      .then(() => console.log("Signed out!"))
  }

  googleLogin(){
    var provider = new firebase.auth.GoogleAuthProvider()
    firebaseUtils.auth().signInWithPopup(provider)
  }

  render() {
    let {members} = this.state;
    let index = 0;
    let content = members ? Object.keys(members).map(memberKey => 
        <li 
          className="ListItem" 
          key={memberKey}
        >
          { index++ + ") " + memberKey + ":"} <h3 className="Member-name">{members[memberKey].email}</h3>
        </li>) : null

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>

        <hr/>
        <ul>
          {content}
        </ul>

        <hr/>
        <div className="Members">
          <h1>add members</h1>
          <input type="text" 
                 value={this.state.email} 
                 onChange={(event) => {
                   this.setState({email: event.target.value})
                   
                 }}/>
          <button onClick={() => this.addMember()}>add item</button>
          <button onClick={() => this.updateValue()}>update a value</button>
          <button onClick={() => this.deleteItem()}>delete an item</button>
        </div>
        
        <div className="Authentication">
          <h1>SignUp/Login</h1>
          <input type="text" 
                 placeholder="username"
                 value={this.state.username} 
                 onChange={(event) => {
                   this.setState({username: event.target.value})
                 }}/>
          <input type="password" 
                 placeholder="password"
                 value={this.state.password} 
                 onChange={(event) => {
                   this.setState({password: event.target.value})
                 }}/>
          <button onClick={() => this.signUp()}>SignUp</button>
          <button onClick={() => this.login()}>Login</button>
          <button onClick={() => this.logout()}>Logout</button>
          <button onClick={() => this.googleLogin()}>GoogleLogin</button>
        </div>
      </div>
    );
  }
}

export default App
